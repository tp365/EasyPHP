<?php
// 记录开始运行时间
$GLOBALS['_beginTime'] = microtime(true);
// 记录内存初始使用
define('MEMORY_LIMIT_ON', function_exists('memory_get_usage'));
if (MEMORY_LIMIT_ON) {
    $GLOBALS['_startUseMems'] = memory_get_usage();
}
// 版本信息
const EASYPHP_VERSION = '1.1.1';
const URL_COMMON      = 0;  //普通模式
const URL_PATHINFO    = 1;  //PATHINFO模式
const URL_REWRITE     = 2;  //REWRITE模式
const URL_COMPAT      = 3;  // 兼容模式
const EXT             = '.class.php';

defined('APP_DEBUG') or define('APP_DEBUG', false);//是否调试
defined('EASY_PATH') or define('EASY_PATH', __DIR__ . '/');
defined('APP_PATH') or define('APP_PATH', dirname($_SERVER['SCRIPT_FILENAME']) . '/');
//defined('MODEL_NAME') or define('MODEL_NAME', 'Web');//项目目录
defined('EASY_LIB_PATH') or define('EASY_LIB_PATH', realpath(EASY_PATH . 'Library') . '/'); // 系统核心类库目录
defined('CORE_PATH') or define('CORE_PATH', EASY_LIB_PATH . 'Easy/'); // Easy类库目录
defined('COMMON_PATH') or define('COMMON_PATH', APP_PATH . 'Common/'); // 应用公共目录
defined('CONF_PATH') or define('CONF_PATH', COMMON_PATH . 'Conf/'); // 应用配置目录
defined('PROJECT_ROOT') or define('PROJECT_ROOT', APP_PATH);
defined('RUNTIME_PATH') or define('RUNTIME_PATH', PROJECT_ROOT . 'Runtime/' . MODEL_NAME . '/');   // 系统运行时目录
defined('TEMP_PATH') or define('TEMP_PATH', APP_PATH . 'Temp/'); // 应用缓存目录
defined('CACHE_PATH') or define('CACHE_PATH', RUNTIME_PATH . 'Cache/'); // 应用模板缓存目录
defined('CONF_EXT') or define('CONF_EXT', '.php'); // 配置文件后缀
defined('CONF_PARSE') or define('CONF_PARSE', '');    // 配置文件解析方法
defined('STORAGE_TYPE') or define('STORAGE_TYPE', 'File'); // 存储类型 默认为File
define('IS_WIN', strstr(PHP_OS, 'WIN') ? 1 : 0);
defined('FRAME_AUTO_RUN') or define('FRAME_AUTO_RUN', true);//是否自动运行

require_once CORE_PATH . 'Easy' . EXT;
require_once CORE_PATH.'LoadClass.class.php';