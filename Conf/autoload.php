<?php
/**
 * EasyPHP 普通模式定义
 */
return array(
    // 函数和类文件
    'core'      =>  array(
        EASY_PATH.'Common/functions.php',
        COMMON_PATH.'function.php',
    ),
	// 配置文件
	'config'    =>  array(
			EASY_PATH.'Conf/config.php',   // 系统惯例配置
			CONF_PATH.'config'.CONF_EXT,      // 应用公共配置
	        APP_PATH.MODEL_NAME.'/Conf/config'.CONF_EXT,      // 项目公共配置
	),
);