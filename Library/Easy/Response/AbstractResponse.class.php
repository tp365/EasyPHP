<?php
namespace Easy\Response;

class AbstractResponse
{
    const SUCCESS = 1;
    const FAILD   = 0;

    const PHP_RUN_IN_FPM = 'fpm-fcgi';

    public static function fail($msg = '', $code = '')
    {
        $resdto = ResponseDTO::arrIns([
            'msg'    => empty($msg) ? '操作失败！' : $msg,
            'status' => self::FAILD,
            'code'   => $code,
        ]);

        return $resdto;
    }

    public static function succ($msg = null, $data = [])
    {
        $resdto = ResponseDTO::arrIns([
            'msg'    => is_null($msg) ? '操作成功！' : $msg,
            'status' => self::SUCCESS,
            'data'   => $data,
        ]);

        return $resdto;
    }

    public static function data($result)
    {
        return $result['data'];
    }

    public static function check(ResponseDTO $responseDTO)
    {
        return $responseDTO->status == self::SUCCESS ? true : false;
    }

    protected static function getDefaultData(ResponseDTO $responseDTO)
    {
        $responseDTO->msg    = !is_null($responseDTO->msg) ? $responseDTO->msg : "操作失败！";
        $responseDTO->status = !is_null($responseDTO->status) ? $responseDTO->status : self::FAILD;
        $responseDTO->data   = !is_null($responseDTO->data) ? $responseDTO->data : [];
        $responseDTO->ret    = $responseDTO->status;

        return $responseDTO;
    }
}