<?php
namespace Easy\Response;

class JsScriptResponse extends AbstractResponse
{
    public static function out(ResponseDTO $responseDTO, $succUrl = '', $failUrl = '')
    {
        if ($responseDTO->code) {
            ResponseCode::headerCode($responseDTO->code);
        }
        $responseDTO = self::getDefaultData($responseDTO);
        if (!self::check($responseDTO->status)) {
            $scripthtml = $failUrl ? "window.location.href='" . $failUrl . "'" : "window.history.go(-1);";
        } else {
            if (!$succUrl) {
                $succUrl = !isset($_SERVER['HTTP_REFERER']) ? "/" : $_SERVER['HTTP_REFERER'];
            }
            $scripthtml = "window.location.href='" . $succUrl . "'";
        }
        $alterString = 'alert("' . $responseDTO->msg . '");';
        echo '<script>' . $alterString . $scripthtml . '</script>';
        exit;
    }
}