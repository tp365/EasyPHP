<?php
namespace Easy\Response;

class ResponseCode
{
    const CODE_200 = 200;

    const CODE_401 = 401;
    const CODE_403 = 403;
    const CODE_404 = 404;

    const CODE_500 = 500;


    public static function headerCode($code)
    {
        switch ($code) {
            case self::CODE_401:
                header("HTTP/1.1 " . $code . " Unauthorized");
                break;
            case self::CODE_403:
                header("HTTP/1.1 " . $code . " Forbidden");
                break;
            case self::CODE_404:
                header("HTTP/1.1 " . $code . "  Not Found");
                break;
            case self::CODE_500:
                header("HTTP/1.1 " . $code . " Internal Server Error");
                break;
            default:
                header("HTTP/1.1 " . self::CODE_200 . " OK");
        }
    }
}