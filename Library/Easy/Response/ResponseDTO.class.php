<?php
namespace Easy\Response;

use Easy\DTO;

class ResponseDTO extends DTO
{
    public $msg;
    public $data;
    public $status;
    public $code;
    public $ret;

    public function getData()
    {
        return $this->data;
    }

    public function toJson()
    {
        if ($this->code == 200 || $this->code == 201) {
            return is_array($this->data) || is_object($this->data) ? json_encode($this->data) : $this->data;
        } else {
            $arr = get_object_vars($this);

            return json_encode($arr);
        }
    }
}