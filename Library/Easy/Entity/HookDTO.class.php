<?php
namespace Easy\Entity;

use Easy\DTO;

class HookDTO extends DTO
{
    public $class;
    public $fetch  = [];
    public $update = [];
    public $insert = [];
    public $isDel  = false;
    public $model;

    public function setModel($model)
    {
        $this->model = $model;
        if($model == Hook::SQL_DELETE){
            $this->isDel = true;
        }
    }

    public function setValue($name, $value)
    {
        switch ($this->model) {
            case Hook::SQL_FETCH:
                $this->pushFetch($name, $value);
                break;
            case Hook::SQL_INSERT:
                $this->pushInsert($name, $value);
                break;
            default:
                $this->pushUpdate($name, $value);
        }
    }

    public function getValue($name)
    {
        return $this->fetch[$name];
    }

    private function pushFetch($name, $value)
    {
        $this->fetch[$name] = $value;
    }

    private function pushUpdate($name, $value)
    {
        $this->fetch[$name]  = $value;
        $this->update[$name] = $value;
    }

    private function pushInsert($name, $value)
    {
        $this->fetch[$name]  = $value;
        $this->insert[$name] = $value;
    }
}