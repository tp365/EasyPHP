<?php

namespace Easy\Entity;

use Easy\DbContainer;

class IDGenter
{
    public static function getLastID($dbInstanceName)//时效性
    {
        $db = DbContainer::get($dbInstanceName);
        $db->table('id_genter')->setInc('id', 1);
        $lastID = $db->fetchOne('select id from id_genter order by id desc');

        return $lastID;
    }
}