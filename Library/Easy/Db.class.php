<?php
namespace Easy;
use Easy\Db\DbDriver;
/**
 * EasyPHP Db模型类
 * 实现了ORM和ActiveRecords模式
 */
class Db {
    // 操作状态
    const MODEL_INSERT          =   1;      //  插入模型数据
    const MODEL_UPDATE          =   2;      //  更新模型数据
    const MODEL_BOTH            =   3;      //  包含上面两种方式
    const MODEL_DELETE          =   4;      //  删除操作
    const MUST_VALIDATE         =   1;      // 必须验证
    const EXISTS_VALIDATE       =   0;      // 表单存在字段则验证
    const VALUE_VALIDATE        =   2;      // 表单值不为空则验证

    // 当前数据库操作对象
    protected $db               =   null;
    // 主键名称
    protected $pk               =   'id';
    // 主键是否自动增长
    protected $autoinc          =   false;    
    // 数据表前缀
    protected $tablePrefix      =   null;
    // 模型名称
    protected $name             =   '';
    // 数据库名称
    protected $dbName           =   '';
    //数据库配置
    protected $connection       =   '';
    // 数据表名（不包含表前缀）
    protected $tableName        =   '';
    // 实际数据表名（包含表前缀）
    protected $trueTableName    =   '';
    // 最近错误信息
    protected $error            =   '';
    // 字段信息
    protected $fields           =   array();
    // 数据信息
    protected $data             =   array();
    // 查询表达式参数
    protected $options          =   array();
    protected $_validate        =   array();  // 自动验证定义
    protected $_auto            =   array();  // 自动完成定义
    protected $_map             =   array();  // 字段映射定义
    protected $_scope           =   array();  // 命名范围定义
    // 是否自动检测数据表字段信息
    protected $autoCheckFields  =   true;
    // 是否批处理验证
    protected $patchValidate    =   false;
    // 链操作方法列表
    protected $methods          =   array('order','alias','having','group','lock','distinct','auto','filter','validate','result','token');

    /**
     * 架构函数
     * 取得DB类的实例对象 字段检查
     * @access public
     * @param string $name 模型名称
     * @param string $tablePrefix 表前缀
     * @param mixed $connection 数据库连接信息
     */
    public function __construct($dbconfig) {
        // 模型初始化
        $this->_initialize();
        // 数据库初始化操作
        // 获取数据库操作对象
        // 当前模型有独立的数据库连接信息
        $this->db($dbconfig,true);
    }

    
    /**
     * 切换当前的数据库连接
     * @access public
     * @param integer $linkNum  连接序号
     * @param mixed $config  数据库连接信息
     * @param boolean $force 强制重新连接
     * @return Db
     */
    public function db($config,$force=false) {
    	if($this->db) {
    		return $this->db;
    	}
    	if($force ) {
    		$this->db            =    DbDriver::getInstance($config);
    	}elseif(NULL === $config){
    		$this->db->close(); // 关闭数据库连接
    		return ;
    	}
    	$this->_after_db();
    	return $this;
    }
    // 数据库切换后回调方法
    protected function _after_db() {}
    
    /**
     * 设置数据对象的值
     * @access public
     * @param string $name 名称
     * @param mixed $value 值
     * @return void
     */
    public function __set($name,$value) {
        // 设置数据对象属性
        $this->data[$name]  =   $value;
    }

    /**
     * 获取数据对象的值
     * @access public
     * @param string $name 名称
     * @return mixed
     */
    public function __get($name) {
        return isset($this->data[$name])?$this->data[$name]:null;
    }

    /**
     * 检测数据对象的值
     * @access public
     * @param string $name 名称
     * @return boolean
     */
    public function __isset($name) {
        return isset($this->data[$name]);
    }

    /**
     * 销毁数据对象的值
     * @access public
     * @param string $name 名称
     * @return void
     */
    public function __unset($name) {
        unset($this->data[$name]);
    }

    /**
     * 利用__call方法实现一些特殊的Model方法
     * @access public
     * @param string $method 方法名称
     * @param array $args 调用参数
     * @return mixed
     */
    public function __call($method,$args) {
        if(in_array(strtolower($method),$this->methods,true)) {
            // 连贯操作的实现
            $this->options[strtolower($method)] =   $args[0];
            return $this;
        }elseif(in_array(strtolower($method),array('count','sum','min','max','avg'),true)){
            // 统计查询的实现
            $field =  isset($args[0])?$args[0]:'*';
            return $this->field(strtoupper($method).'('.$field.') AS ep_'.$method)->fetchOne();
        }else{
            E(__CLASS__.':'.$method.'_METHOD_NOT_EXIST_');
            return;
        }
    }
    // 回调方法 初始化模型
    protected function _initialize() {}

    
    /**
     * 新增数据
     * @access public
     * @param mixed $data 数据
     * @param array $options 表达式
     * @param boolean $replace 是否replace
     * @return mixed
     */
    public function insert($data='',$options=array(),$replace=false) {
    	if(empty($data)) {
    		// 没有传递数据，获取当前数据对象的值
    		if(!empty($this->data)) {
    			$data           =   $this->data;
    			// 重置数据
    			$this->data     = array();
    		}else{
    			$this->error    = E('_DATA_TYPE_INVALID_');
    			return false;
    		}
    	}
    	// 分析表达式
    	$options    =   $this->_parseOptions($options);
    	// 写入数据到数据库
    	return $this->db->insert($options['table'],$data);
    }
    
    /**
     * 保存数据
     * @access public
     * @param mixed $data 数据
     * @param array $options 表达式
     * @return boolean
     */
    public function update($data='',$options=array()) {
    	if(empty($data)) {
    		// 没有传递数据，获取当前数据对象的值
    		if(!empty($this->data)) {
    			$data           =   $this->data;
    			// 重置数据
    			$this->data     =   array();
    		}else{
    			$this->error    =   E('_DATA_TYPE_INVALID_');
    			return false;
    		}
    	}
    	if(empty($data)){
    		// 没有数据则不执行
    		$this->error    =   E('_DATA_TYPE_INVALID_');
    		return false;
    	}
    	// 分析表达式
    	$options    =   $this->_parseOptions($options);
    	
    	if(!isset($options['where']) ) {
    		// 如果没有任何更新条件则不执行
    		$this->error        =   E('_OPERATION_WRONG_NO_WHERE_');
    	}
    	$options['data'] = $data;
    	$options['option_type'] = self::MODEL_UPDATE;
    	$options['limit'] = !isset($options['limit'])?1:$options['limit'];
    	$sql = $this->buildSql($options);
    	return $this->db->update($sql);
    }
    
    /**
     * 设置记录的某个字段值
     * 支持使用数据库字段和方法
     * @access public
     * @param string|array $field  字段名
     * @param string $value  字段值
     * @return boolean
     */
    public function setField($field,$value='') {
        if(is_array($field)) {
            $data           =   $field;
        }else{
            $data[$field]   =   $value;
        }
        return $this->update($data);
    }

    /**
     * 字段值增长
     * @access public
     * @param string $field  字段名
     * @param integer $step  增长值
     * @return boolean
     */
    public function setInc($field,$step=1) {
        return $this->setField($field,array('exp',$field.'+'.$step));
    }

    /**
     * 字段值减少
     * @access public
     * @param string $field  字段名
     * @param integer $step  减少值
     * @return boolean
     */
    public function setDec($field,$step=1) {
        return $this->setField($field,array('exp',$field.'-'.$step));
    }

    /**
     * 删除数据
     * @access public
     * @param mixed $options 表达式
     * @return mixed
     */
    public function delete($options=array()) {
    	// 分析表达式
    	$options =  $this->_parseOptions($options);
    	if(empty($options['where'])){
    		// 如果条件为空 不进行删除操作 除非设置 1=1
    		return false;
    	}
    	if(!isset($options['where']) ) {
    		// 如果没有任何更新条件则不执行
    		$this->error        =   E('_OPERATION_WRONG_NO_WHERE_');
    	}
    	$options['option_type'] = self::MODEL_DELETE;
    	$options['limit'] = !isset($options['limit'])?1:$options['limit'];
    	$sql = $this->buildSql($options);
    	return $this->db->delete($sql);
    }
    
    /**
     * 取回结果集中所有字段的值,作为连续数组返回
     * @param string $sql
     * @param unknown $bind
     * @param string $fetchMode
     */
    public function fetchAll($sql = '',$bind = array(), $fetchMode = null){
    	$sql = empty($sql)?$this->buildSql():$sql;
    	return $this->db->fetchAll($sql,$bind,$fetchMode);
    }

    /**
     * 只取回结果集的第一行
     * @param unknown $sql
     * @param unknown $bind
     * @param string $fetchMode
     */
    public function fetchRow($sql='', $bind = array(), $fetchMode = null){
    	$sql = empty($sql)?$this->buildSql():$sql;
    	return $this->db->fetchRow($sql,$bind,$fetchMode);
    }
    
    /**
     * 取回所有结果行的第一个字段名
     * @param unknown $sql
     * @param unknown $bind
     */
    public function fetchCol($sql='', $bind = array()){
    	$sql = empty($sql)?$this->buildSql():$sql;
    	return $this->db->fetchCol($sql,$bind);
    }
    
    /**
     * 只取回第一个字段值
     * @param unknown $sql
     * @param unknown $bind
     */
    public function fetchOne($sql='', $bind = array()){
    	$sql = empty($sql)?$this->buildSql():$sql;
    	return $this->db->fetchOne($sql,$bind);
    }
	
    /**
     * 取回一个相关数组,第一个字段值为码 第二个字段为值
     * @param unknown $sql
     * @param unknown $bind
     */
    public function fetchPairs($sql='', $bind = array()){
    	$sql = empty($sql)?$this->buildSql():$sql;
    	return $this->db->fetchPairs($sql,$bind);
    }
    
    /**
     * 复杂sql语句，可直接执行query
     * @param unknown $sql
     * @param unknown $bind
     */
    public function query($sql,$bind=array(),$fetchMode=null){
    	return $this->db->execute($sql,$bind,$fetchMode);
    }
    
    /**
     * 启动事务
     * @access public
     * @return void
     */
    public function startTrans() {
        $this->commit();
        $this->db->startTrans();
        return ;
    }

    /**
     * 提交事务
     * @access public
     * @return boolean
     */
    public function commit() {
        return $this->db->commit();
    }

    /**
     * 事务回滚
     * @access public
     * @return boolean
     */
    public function rollback() {
        return $this->db->rollback();
    }

    /**
     * 返回模型的错误信息
     * @access public
     * @return string
     */
    public function getError(){
        return $this->error;
    }


    /**
     * 返回最后执行的sql语句
     * @access public
     * @return string
     */
    public function getLastSql() {
        return $this->db->getLastSql();
    }

    /**
     * 设置数据对象值
     * @access public
     * @param mixed $data 数据
     * @return Db
     */
    public function data($data=''){
        if('' === $data && !empty($this->data)) {
            return $this->data;
        }
        if(is_object($data)){
            $data   =   get_object_vars($data);
        }elseif(is_string($data)){
            parse_str($data,$data);
        }elseif(!is_array($data)){
            E('_DATA_TYPE_INVALID_');
        }
        $this->data = $data;
        return $this;
    }


    /**
     * 查询缓存
     * @access public
     * @param mixed $key
     * @param integer $expire
     * @param string $type
     * @return Db
     */
    public function cache($key=true,$expire=null,$type=''){
        if(false !== $key)
            $this->options['cache']  =  array('key'=>$key,'expire'=>$expire,'type'=>$type);
        return $this;
    }
    
    /**
     * 生成查询SQL 可用于子查询
     * @access public
     * @param array $options 表达式参数
     * @return string
     */
    public function buildSql($options=array()) {
    	// 分析表达式
    	$options =  $this->_parseOptions($options);
    	if(isset($options['option_type']) && $options['option_type'] == self::MODEL_UPDATE){
    		return  $this->db->buildUpdateSql($options);
    	}elseif(isset($options['option_type']) && $options['option_type'] == self::MODEL_DELETE){
    		return  $this->db->buildDeleteSql($options);
    	}else{
    		return  '( '.$this->db->buildSelectSql($options).' )';
    	}
    }
    
    
    /**
     * 分析表达式
     * @access protected
     * @param array $options 表达式参数
     * @return array
     */
    protected function _parseOptions($options=array()) {
    	if(is_array($options))
    		$options =  array_merge($this->options,$options);
    	if(!isset($options['table'])){
    		E('table not selected');
    	}
    
    	// 数据表别名
    	if(!empty($options['alias'])) {
    		$options['table']  .=   ' '.$options['alias'];
    	}
    	// 记录操作的模型名称
    	$options['model']       =   $this->name;
    	
    	// 查询过后清空sql表达式组装 避免影响下次查询
    	$this->options  =   array();
    	// 表达式过滤
    	$this->_options_filter($options);
    	return $options;
    }
    // 表达式过滤回调方法
    protected function _options_filter(&$options) {}
    
    
    
    /**
     * 指定当前的数据表
     * @access public
     * @param mixed $table
     * @return Db
     */
    public function table($table) {
    	$prefix =   $this->tablePrefix;
    	if(is_array($table)) {
    		$this->options['table'] =   $table;
    	}elseif(!empty($table)) {
    		//将__TABLE_NAME__替换成带前缀的表名
    		$table  = preg_replace_callback("/__([A-Z_-]+)__/sU", function($match) use($prefix){ return $prefix.strtolower($match[1]);}, $table);
    		$this->options['table'] =   $table;
    	}
    	return $this;
    }
    
    /**
     * 指定查询条件 支持安全过滤
     * @access public
     * @param mixed $where 条件表达式
     * @param mixed $parse 预处理参数
     * @return Db
     */
    public function where($where,$parse=null){
        if(!is_null($parse) && is_string($where)) {
            if(!is_array($parse)) {
                $parse = func_get_args();
                array_shift($parse);
            }
            $parse = array_map(array($this->db,'escapeString'),$parse);
            $where =   vsprintf($where,$parse);
        }elseif(is_object($where)){
            $where  =   get_object_vars($where);
        }
        if(is_string($where) && '' != $where){
            $map    =   array();
            $map['_string']   =   $where;
            $where  =   $map;
        }        
        if(isset($this->options['where'])){
            $this->options['where'] =   array_merge($this->options['where'],$where);
        }else{
            $this->options['where'] =   $where;
        }
        
        return $this;
    }

    /**
     * 指定查询字段 支持字段排除
     * @access public
     * @param mixed $field
     * @param boolean $except 是否排除
     * @return Db
     */
    public function field($field,$except=false){
    	if(true === $field) {// 获取全部字段
    		$fields     =  $this->getDbFields();
    		$field      =  $fields?:'*';
    	}elseif($except) {// 字段排除
    		if(is_string($field)) {
    			$field  =  explode(',',$field);
    		}
    		$fields     =  $this->getDbFields();
    		$field      =  $fields?array_diff($fields,$field):$field;
    	}
    	$this->options['field']   =   $field;
    	return $this;
    }
    
    /**
     * 指定查询数量
     * @access public
     * @param mixed $offset 起始位置
     * @param mixed $length 查询数量
     * @return Db
     */
    public function limit($offset,$length=null){
        $this->options['limit'] =   is_null($length)?$offset:$offset.','.$length;
        return $this;
    }

    /**
     * 指定分页
     * @access public
     * @param mixed $page 页数
     * @param mixed $listRows 每页数量
     * @return Db
     */
    public function page($page,$listRows=null){
        $this->options['page'] =   is_null($listRows)?$page:$page.','.$listRows;
        return $this;
    }

    /**
     * 查询注释
     * @access public
     * @param string $comment 注释
     * @return Db
     */
    public function comment($comment){
        $this->options['comment'] =   $comment;
        return $this;
    }

    /**
     * 参数绑定
     * @access public
     * @param string $key  参数名
     * @param mixed $value  绑定的变量及绑定参数
     * @return Db
     */
    public function bind($key,$value=false) {
        if(is_array($key)){
            $this->options['bind'] =    $key;
        }else{
            $num =  func_num_args();
            if($num>2){
                $params =   func_get_args();
                array_shift($params);
                $this->options['bind'][$key] =  $params;
            }else{
                $this->options['bind'][$key] =  $value;
            }        
        }
        return $this;
    }

    /**
     * 设置模型的属性值
     * @access public
     * @param string $name 名称
     * @param mixed $value 值
     * @return Db
     */
    public function setProperty($name,$value) {
        if(property_exists($this,$name))
            $this->$name = $value;
        return $this;
    }

}
