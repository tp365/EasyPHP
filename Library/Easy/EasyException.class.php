<?php
namespace Easy;
class EasyException extends \Exception
{
    public function __construct($msg = '',$tag='',$code = 0, Exception $previous = null)
    {
        EasyLog::log($msg,$tag,EasyLog::EXCE);
        parent::__construct($msg.'['.$tag.']', (int) $code, $previous);
    }
}

