<?php
namespace Easy\Exception;

use Easy\EasyLog;
use Easy\Response\JsonResponse;
use Easy\Response\ResponseCode;

class LogicError extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
        $resDTO = JsonResponse::fail($message, ResponseCode::CODE_500);
        EasyLog::log($resDTO->msg, 'error', EasyLog::ERR);
        JsonResponse::out($resDTO);
    }
}