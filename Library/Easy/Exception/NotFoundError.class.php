<?php
namespace Easy\Exception;

use Easy\Response\JsonResponse;
use Easy\Response\ResponseCode;

class NotFoundError extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
        $resDTO = JsonResponse::fail($message, ResponseCode::CODE_404);
        JsonResponse::out($resDTO);
    }
}