<?php
namespace Easy;

class EasyLog
{
    const MSG  = 'msg';
    const ERR  = 'err';
    const EXCE = 'exce';//exception
    const LOG  = 'log';

    public static function log($msg, $tag, $level = self::MSG)
    {
        $path = APPLICATION_ROOT . '/Log';
        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }
        $path .= '/' . MODEL_NAME;
        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }
        $path .= '/' . date("Ym");
        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }
        $logFile = $path . '/' . date('Ymd') . '.' . $level;
        $msg     = date("Y-m-d H:i:s") . "\t" . '[' . $tag . ']' . $msg . PHP_EOL;

        $file = fopen($logFile, "a+");
        fwrite($file, $msg . PHP_EOL);
        fclose($file);
    }
}