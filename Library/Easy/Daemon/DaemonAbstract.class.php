<?php
namespace Easy\Daemon;
class DaemonAbstract
{
    public function __construct($method,$param, $config)
    {
        $this->_beforeInit($param, $config);
        if ($config['static']) {
            while (1) {
                $this->$method($param, $config);
                usleep($config['sleep'] * 1000000);
            }
        } else {
            $this->$method($param, $config);
            usleep($config['sleep'] * 1000000);
        }
    }

    protected function _beforeInit($param = null, $config = null)
    {

    }
}
