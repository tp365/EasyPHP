<?php
namespace Easy\Http;

class Response
{
    private $statusCode;
    private $body;

    public function __construct($code, $body)
    {
        $this->statusCode = $code;
        $this->body       = $body;
    }

    public function get()
    {
        return $this->body;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getFromJson()
    {
        $data = json_decode($this->body, true);

        return $data ? $data : $this->body;
    }
}