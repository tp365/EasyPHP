<?php
namespace Easy\Http;

use Easy\DTO;

class RequestDTO extends DTO
{
    public $domain;
    public $isHttps = false;
    public $port    = 80;
    public $timeout = 5;
    public $gzip    = true;
    public $exception;

    public function getUrl($url)
    {
        if ($this->isHttps) {
            return $url = "https://" . $this->domain . $url;
        }
        if ($this->port && $this->port != 80) {
            $url = "http://{$this->domain}:{$this->port}{$url}";
        } else {
            $url = "http://" . $this->domain . $url;
        }

        return $url;
    }
}