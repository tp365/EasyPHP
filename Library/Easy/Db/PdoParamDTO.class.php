<?php
namespace Easy\Db;

class PdoParamDTO extends DbDTO
{
    public $persistent;//长连接
    public $prepare;//是否预处理  
    public $charset;//编码
}