<?php
namespace Easy\Db;
use Easy\DTO;
class DbDTO extends DTO
{
    public $masterHost;
    public $slaveHost;
    public $name;
    public $user;
    public $passwd;
    public $port;
    public $charset;
}

