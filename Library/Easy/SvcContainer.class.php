<?php

namespace Easy;

use Easy\Exception\LogicError;

class SvcContainer
{
    private static $_svc;

    public static function regist($svcName, $clsSvc)
    {
        if (isset(self::$_svc[$svcName])) {
            throw new LogicError($svcName . ' had regist');
        }
        self::$_svc[$svcName] = $clsSvc;
    }

    public static function get($svcName)
    {
        if (!isset(self::$_svc[$svcName])) {
            throw new LogicError($svcName . ' not regist');
        }

        return self::$_svc[$svcName];
    }
}